provider "hcloud" {
}

data "hcloud_image" "master_image" {
  with_selector = "image == gitlab-runner-master"
  most_recent   = "true"
}

data "hcloud_image" "minion_image" {
  with_selector = "image == gitlab-runner-minion"
  most_recent   = "true"
}

resource "tls_private_key" "ssh_key" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "hcloud_ssh_key" "terraform_key" {
  name       = "terraform"
  public_key = tls_private_key.ssh_key.public_key_openssh
}

resource "hcloud_volume" "cache_volume" {
  name      = "gitlab-runner-cache"
  size      = 10
  server_id = hcloud_server.master.id
  format    = "ext4"
}

resource "hcloud_network" "runner-net" {
  name     = "runner-net"
  ip_range = "10.0.0.0/24"
}

resource "hcloud_network_subnet" "runner-subnet" {
  network_id   = hcloud_network.runner-net.id
  type         = "server"
  network_zone = "eu-central"
  ip_range     = "10.0.0.0/24"
}

resource "hcloud_server_network" "master_ip" {
  server_id  = hcloud_server.master.id
  network_id = hcloud_network.runner-net.id
  ip         = "10.0.0.2"
}

resource "hcloud_server" "master" {
  name        = "master-${data.hcloud_image.minion_image.id}"
  image       = data.hcloud_image.master_image.id
  server_type = "cx11"
  location    = "nbg1"
  ssh_keys = [
    hcloud_ssh_key.terraform_key.id,
  ]

  connection {
    type        = "ssh"
    user        = "root"
    host        = hcloud_server.master.ipv4_address
    private_key = tls_private_key.ssh_key.private_key_pem
  }

  provisioner "remote-exec" {
    when = create
    inline = [
      "/bin/sed -i 's/MINION_IMAGE_ID/${data.hcloud_image.minion_image.id}/g' /usr/local/sbin/register-runner.sh",
      "/usr/local/sbin/register-runner.sh",
    ]
  }

  provisioner "remote-exec" {
    when = destroy
    inline = [
      "/usr/local/sbin/unregister-runner.sh",
    ]
  }
}

terraform {
  backend "remote" {
    organization = "sshoney"

    workspaces {
      name = "gitlab-runner"
    }
  }
}
