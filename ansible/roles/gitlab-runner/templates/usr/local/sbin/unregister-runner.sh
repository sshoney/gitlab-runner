#!/bin/bash
# {{ ansible_managed }}

/bin/systemctl stop gitlab-runner

{% raw %}
for machine in $(/usr/local/bin/docker-machine ls -f '{{.Name}}')
{% endraw %}
do
	/usr/local/bin/docker-machine kill $machine
	/usr/local/bin/docker-machine rm --force $machine
done

/usr/bin/gitlab-runner unregister --all-runners
