#!/bin/bash
# {{ ansible_managed }}

# We want to know how much entropy we have for debugging
/bin/echo "$(/bin/cat /proc/sys/kernel/random/entropy_avail)"

/bin/rm -rf /etc/gitlab-runner/config.toml

/usr/bin/gitlab-runner register \
    --executor docker+machine \
    --non-interactive \
    --registration-token "{{ lookup('env', 'GITLAB_TOKEN') }}" \
    --run-untagged \
    --tag-list "ruby,sshoney,docker" \
    --url https://gitlab.com \
    --cache-s3-access-key "{{ minio_access_key }}" \
    --cache-s3-bucket-name runner \
    --cache-s3-insecure \
    --cache-s3-secret-key "{{ minio_secret_key }}" \
    --cache-s3-server-address "10.0.0.2:9000" \
    --cache-shared \
    --cache-type s3 \
    --docker-image "docker:latest" \
    --docker-privileged \
    --docker-pull-policy if-not-present \
    --docker-shm-size 268435456 \
    --docker-tlsverify \
    --docker-volumes "/certs/client" \
    --docker-volumes "/cache" \
    --machine-idle-nodes 0 \
    --machine-idle-time 7200 \
    --machine-machine-driver "hetzner" \
    --machine-machine-name "MINION_IMAGE_ID-%s" \
    --machine-machine-options "engine-registry-mirror=http://10.0.0.2:6000" \
    --machine-machine-options "hetzner-api-token={{ lookup('env', 'HCLOUD_TOKEN') }}" \
    --machine-machine-options "hetzner-image-id=MINION_IMAGE_ID" \
    --machine-machine-options "hetzner-networks=runner-net" \
    --machine-machine-options "hetzner-server-type=cx11" \
    --machine-max-builds 25

/usr/bin/gitlab-runner register \
    --executor shell \
    --non-interactive \
    --registration-token "{{ lookup('env', 'GITLAB_TOKEN') }}" \
    --run-untagged \
    --tag-list "ruby,sshoney,shell" \
    --url https://gitlab.com \
    --cache-s3-access-key "{{ minio_access_key }}" \
    --cache-s3-bucket-name runner \
    --cache-s3-insecure \
    --cache-s3-secret-key "{{ minio_secret_key }}" \
    --cache-s3-server-address "10.0.0.2:9000" \
    --cache-shared \
    --cache-type s3

/usr/bin/sed -i 's/concurrent = [0-9]\+/concurrent = 10/' /etc/gitlab-runner/config.toml
/bin/systemctl restart gitlab-runner
