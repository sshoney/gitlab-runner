# Autoscaling gitlab-runners with Hetzner Cloud

This project will install autoscaling gitlab-runners using Hetzner Cloud

To get this working you will need:

- Gitlab account
- Hetzner Cloud account
- Terraform Cloud account

Create this protected and masked variables inside gitlab:

  - `HCLOUD_TOKEN`, your Hetzner Cloud access token
  - `GITLAB_TOKEN` your gitlab-runner registration token
  - `TERRAFORM_CLOUD_TOKEN_A` your terraform access token part_a (eg. `<part_a>.atlassv1.<part_b>`)
  - `TERRAFORM_CLOUD_TOKEN_B` your terraform access token part_b (eg. `<part_a>.atlassv1.<part_b>`)

**Note** The splitting of the `TERRAFORM_CLOUD_TOKEN`is needed because Gitlab can't mask a value
containing a dot. 

Setup a CI/CD => schedule when the gitlab-runner infrastructure should be replaced with fresh images.

To change the gitlab-runner config make your changes [here](https://gitlab.com/sshoney/gitlab-runner/blob/master/ansible/roles/gitlab-runner/templates/usr/local/sbin/register-runner.sh).

If you clone this project and start from scratch you will run into a chicken vs egg problem.
Every job which is able to run on your runners should be run there. But if there are none you need
to use your currents one or the shared runners from gitlab.org.
Only the job which is going to apply the changes to your infrastructure is not allowed to be
running later on on your runners.

To avoid the chicken vs egg problem follow this steps:

  1. Make sure that every job in your CI is running on gitlabs shared runners or a static one which
you don't replace
  2. Ensure that the variables mentioned above are available to your pipeline
  3. Fire up the pipeline
  4. Change every job to run on your new runners except for the apply job
