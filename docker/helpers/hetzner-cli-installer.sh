#!/bin/bash

mkdir -p /tmp/hetzner-cli
cd /tmp/hetzner-cli || exit 1

VERSION="$(curl -sI "https://github.com/hetznercloud/cli/releases/latest" | grep location | grep -Po '[0-9]\.[0-9]{1,2}\.[0-9]+')"
curl -C - -OJLs "https://github.com/hetznercloud/cli/releases/download/v${VERSION}/hcloud-linux-amd64.tar.gz"
tar -xzvf "hcloud-linux-amd64.tar.gz"
cp hcloud /usr/local/bin
