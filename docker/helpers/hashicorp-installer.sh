#!/bin/bash

PUB_KEY="""
-----BEGIN PGP PUBLIC KEY BLOCK-----
Version: GnuPG v1

mQENBFMORM0BCADBRyKO1MhCirazOSVwcfTr1xUxjPvfxD3hjUwHtjsOy/bT6p9f
W2mRPfwnq2JB5As+paL3UGDsSRDnK9KAxQb0NNF4+eVhr/EJ18s3wwXXDMjpIifq
fIm2WyH3G+aRLTLPIpscUNKDyxFOUbsmgXAmJ46Re1fn8uKxKRHbfa39aeuEYWFA
3drdL1WoUngvED7f+RnKBK2G6ZEpO+LDovQk19xGjiMTtPJrjMjZJ3QXqPvx5wca
KSZLr4lMTuoTI/ZXyZy5bD4tShiZz6KcyX27cD70q2iRcEZ0poLKHyEIDAi3TM5k
SwbbWBFd5RNPOR0qzrb/0p9ksKK48IIfH2FvABEBAAG0K0hhc2hpQ29ycCBTZWN1
cml0eSA8c2VjdXJpdHlAaGFzaGljb3JwLmNvbT6JATgEEwECACIFAlMORM0CGwMG
CwkIBwMCBhUIAgkKCwQWAgMBAh4BAheAAAoJEFGFLYc0j/xMyWIIAIPhcVqiQ59n
Jc07gjUX0SWBJAxEG1lKxfzS4Xp+57h2xxTpdotGQ1fZwsihaIqow337YHQI3q0i
SqV534Ms+j/tU7X8sq11xFJIeEVG8PASRCwmryUwghFKPlHETQ8jJ+Y8+1asRydi
psP3B/5Mjhqv/uOK+Vy3zAyIpyDOMtIpOVfjSpCplVRdtSTFWBu9Em7j5I2HMn1w
sJZnJgXKpybpibGiiTtmnFLOwibmprSu04rsnP4ncdC2XRD4wIjoyA+4PKgX3sCO
klEzKryWYBmLkJOMDdo52LttP3279s7XrkLEE7ia0fXa2c12EQ0f0DQ1tGUvyVEW
WmJVccm5bq25AQ0EUw5EzQEIANaPUY04/g7AmYkOMjaCZ6iTp9hB5Rsj/4ee/ln9
wArzRO9+3eejLWh53FoN1rO+su7tiXJA5YAzVy6tuolrqjM8DBztPxdLBbEi4V+j
2tK0dATdBQBHEh3OJApO2UBtcjaZBT31zrG9K55D+CrcgIVEHAKY8Cb4kLBkb5wM
skn+DrASKU0BNIV1qRsxfiUdQHZfSqtp004nrql1lbFMLFEuiY8FZrkkQ9qduixo
mTT6f34/oiY+Jam3zCK7RDN/OjuWheIPGj/Qbx9JuNiwgX6yRj7OE1tjUx6d8g9y
0H1fmLJbb3WZZbuuGFnK6qrE3bGeY8+AWaJAZ37wpWh1p0cAEQEAAYkBHwQYAQIA
CQUCUw5EzQIbDAAKCRBRhS2HNI/8TJntCAClU7TOO/X053eKF1jqNW4A1qpxctVc
z8eTcY8Om5O4f6a/rfxfNFKn9Qyja/OG1xWNobETy7MiMXYjaa8uUx5iFy6kMVaP
0BXJ59NLZjMARGw6lVTYDTIvzqqqwLxgliSDfSnqUhubGwvykANPO+93BBx89MRG
unNoYGXtPlhNFrAsB1VR8+EyKLv2HQtGCPSFBhrjuzH3gxGibNDDdFQLxxuJWepJ
EK1UbTS4ms0NgZ2Uknqn1WRU1Ki7rE4sTy68iZtWpKQXZEJa0IGnuI2sSINGcXCJ
oEIgXTMyCILo34Fa/C6VCm2WBgz9zZO8/rHIiQm1J5zqz0DrDwKBUM9C
=LYpS
-----END PGP PUBLIC KEY BLOCK-----
"""

gpg --import <(echo "${PUB_KEY}")

if [ $# -eq 0 ]; then
  echo "No tools as arguments found"
  exit 1
fi

for TOOL in "$@"; do
  if [[ "${TOOL}" != "packer" && "${TOOL}" != "terraform" ]]; then
    echo "${TOOL} is not a valid tool for installing"
    exit 1
  fi

  VERSION="$(curl -sI "https://github.com/hashicorp/${TOOL}/releases/latest" | grep location | grep -Po '[0-9]\.[0-9]{1,2}\.[0-9]+')"
  BASE_DOWNLOAD="https://releases.hashicorp.com/${TOOL}/${VERSION}/"
  TOOL_FILE="${TOOL}_${VERSION}_linux_amd64.zip"
  TOOL_SUM="${TOOL}_${VERSION}_SHA256SUMS"
  TOOL_SIG="${TOOL}_${VERSION}_SHA256SUMS.sig"
  BUILD_DIR="/tmp/${TOOL}"

  mkdir -p "${BUILD_DIR}"
  cd "${BUILD_DIR}" || exit 1

  for x in "${TOOL_FILE}" "${TOOL_SUM}" "${TOOL_SIG}"; do
    echo "${BASE_DOWNLOAD}${x}"
    curl -C - -OJLs "${BASE_DOWNLOAD}${x}"
  done

  gpg --trust-model always --verify "${TOOL_SIG}" || (echo "Verification of GPG sig failed" && exit 1)
  grep "${TOOL_FILE}" "${TOOL_SUM}" | sha256sum -c - || (echo "Verification of SHA2 failed" && exit 1)
  unzip -o "${TOOL_FILE}" -d /usr/local/bin
done
